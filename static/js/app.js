var app4 = new Vue({
    el: '#app-4',
    data: {
        machines: [],
        users: [],
        userMachineDate: [],
        div1show: true,
        div2show: false,
        div3show: false,
        logData: [],
        yearsList: [],
//        monthList: ['January','February','March','April','May','June','July','August','September','October','November','December']
        monthList: ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'],

        selMachID: 1,
        selUserID: 1,
		selMonth: -1,
		selYear: -1
    },
    watch: {
        selYear: function (val, oldVal) {
            console.log('новое значение: %s, старое значение: %s', val, oldVal)
        },

        },
    methods: {
        readResult: function() {
            var dataURL = '/api/1.0/get_machines';
            axios
                .get(dataURL)
                .then(response => (this.machines = response.data));

            dataURL = '/api/1.0/get_users';
            axios
                .get(dataURL)
                .then(response => (this.users = response.data));

            dataURL = '/api/1.0/get_years';
            axios
                .get(dataURL)
                .then(response => (this.yearsList = response.data));
        },
        loadLog: function(id){
            var dataURL = '/api/1.0/get_log?mach_id=' + id;
            axios
                .get(dataURL)
                .then(response => (this.logData = response.data));
            this.selMachID = id;
        },
        loadUserMachineData: function(user,month,year){
            var dataURL =   '/api/1.0/get_user_hours?' +
                            '&user_id=' + user +
                            '&year=' + year +
                            '&month=' + month;
            axios
                .get(dataURL)
                .then(response => (this.userMachineDate = response.data));
            console.log(this.userMachineDate)
        },
        showDiv: function(id) {
            switch(id) {
                case 1:
                    this.div1show = true;
                    this.div2show = false;
                    this.div3show = false;
                    break;
                case 2:
                    this.div1show = false;
                    this.div2show = true;
                    this.div3show = false;
                    break;
                case 3:
                    this.div1show = false;
                    this.div2show = false;
                    this.div3show = true;
                    this.loadLog(1);
                    break;
            }
        },
    },
    filters: {

    },
    watch: {
        selMonth: function (val, oldVal) {
            this.loadUserMachineData(this.selUserID,this.selMonth+1,this.selYear);
        },
        selYear: function (val, oldVal) {
            this.loadUserMachineData(this.selUserID,this.selMonth+1,this.selYear);
        }
    },
    mounted() {
        this.readResult();
    }
})
